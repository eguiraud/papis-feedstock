# papis-feedstock<br>
[![install with conda](https://anaconda.org/bluehood/papis/badges/installer/conda.svg)](https://anaconda.org/bluehood/papis)
[![last_built](https://anaconda.org/bluehood/papis/badges/latest_release_date.svg)](https://anaconda.org/bluehood/papis)
[![license](https://anaconda.org/bluehood/papis/badges/license.svg)](https://anaconda.org/bluehood/papis)

I am not related to papis' development in any way, I just created a conda package for it.
Refer to the [project homepage](https://github.com/papis/papis) for questions or bug reports.

## Conda installation
```bash
$ conda install -c conda-forge -c bluehood papis
```